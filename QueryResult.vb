﻿Option Strict On
' Сделано в SharpDevelop.
' Пользователь: Админ
' Дата: 24.05.2017
' Время: 11:33
' 
' Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
'
#Region "Imports directives"
Imports System.Runtime.InteropServices
Imports System.Globalization
#End Region

''' <summary>
''' Результат выполнения запроса к сервису.
''' </summary>
<ComClass(QueryResult.ClassId, QueryResult.InterfaceId, _
          QueryResult.EventsId), ComVisible(True)> _
Public Class QueryResult
	
#Region "COM Registration"
    Public Const ClassId As String _
    = "5fab60e4-050c-4b4f-9111-78c59ca1b23f"
    Public Const InterfaceId As String _
    = "f7146e24-5c76-472d-883d-b23e3848a2b0"
    Public Const EventsId As String _
    = "498ac43e-ae72-4573-a55c-31aece0afd59"
#End Region
	
	Public Sub New()
  	  MyBase.New()
	End Sub
	
	Private m_ErrorHasOccurred As Boolean = False
	''' <summary>
	''' Индикатор ошибки во время выполнения операции
	''' </summary>
	Public Property ErrorHasOccurred() As Boolean
		Get
			Return m_ErrorHasOccurred
		End Get
		Set
			m_ErrorHasOccurred = value
		End Set
	End Property

	
	Private m_ErrorMessage As String = ""
	''' <summary>
	''' Текст сообщения об ошибке
	''' </summary>
	Public Property ErrorMessage() As String
		Get
			Return m_ErrorMessage
		End Get
		Set
			m_ErrorMessage = value
		End Set
	End Property
	
	Private m_IsCompany As Boolean
	''' <summary>
	''' Это юридическое лицо
	''' </summary>
	Public Property IsCompany() As Boolean
		Get
			Return m_IsCompany
		End Get
		Set
			m_IsCompany = value
		End Set
	End Property
	
	Private m_IsEntrepreneur As Boolean
	''' <summary>
	''' Это индивидуальный предприниматель 
	''' </summary>
	Public Property IsEntrepreneur() As Boolean
		Get
			Return m_IsEntrepreneur
		End Get
		Set
			m_IsEntrepreneur = value
		End Set
	End Property
	
	Private m_INN As String = ""
	Public Property INN() As String
		Get
			Return m_INN
		End Get
		Set
			m_INN = value
		End Set
	End Property
	
	Private m_KPP As String = ""
	Public Property KPP() As String
		Get
			Return m_KPP
		End Get
		Set
			m_KPP = value
		End Set
	End Property
	
	Private m_OGRN As String = ""
	Public Property OGRN() As String
		Get
			Return m_OGRN
		End Get
		Set
			m_OGRN = value
		End Set
	End Property
	
	Private m_FirstName As String = ""
	''' <summary>
	''' Имя ИП
	''' </summary>
	Public Property FirstName() As String
		Get
			Return m_FirstName
		End Get
		Set
			m_FirstName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value.ToLower)
		End Set
	End Property
	
	Private m_MiddleName As String = ""
	''' <summary>
	''' Отчество ИП
	''' </summary>
	Public Property MiddleName() As String
		Get
			Return m_MiddleName
		End Get
		Set
			m_MiddleName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value.ToLower)
		End Set
	End Property
	
	Private m_LastName As String = ""
	''' <summary>
	''' Фамилия ИП
	''' </summary>
	Public Property LastName() As String
		Get
			Return m_LastName
		End Get
		Set
			m_LastName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value.ToLower)
		End Set
	End Property
	
	Private m_Sex As Integer = 0
	''' <summary>
	''' Пол физлица 
	''' </summary>
	''' <value>Для организаций - 0. Для ИП: 1 - мужской, 2 - женский</value>
	Public Property Sex() As Integer
		Get
			Return m_Sex
		End Get
		Set
			m_Sex = value
		End Set
	End Property
	
	Private m_Title As String = ""
	''' <summary>
	''' Наименование
	''' </summary>
	Public Property Title() As String
		Get
			Return m_Title
		End Get
		Set
			m_Title = value
		End Set
	End Property
	
	Private m_FullTitle As String = ""
	''' <summary>
	''' Полное наименование
	''' </summary>
	Public Property FullTitle() As String
		Get
			Return m_FullTitle
		End Get
		Set
			m_FullTitle = value
		End Set
	End Property
	
	Private m_DirectorPosition As String = ""
	''' <summary>
	''' Должность руководителя
	''' </summary>
	Public Property DirectorPosition() As String
		Get
			Return m_DirectorPosition
		End Get
		Set
			m_DirectorPosition = value.ToLower
		End Set
	End Property
	Private m_DirectorFirstName As String = ""
	''' <summary>
	''' Имя руководителя
	''' </summary>
	Public Property DirectorFirstName() As String
		Get
			Return m_DirectorFirstName
		End Get
		Set
			m_DirectorFirstName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value.ToLower)
		End Set
	End Property
	Private m_DirectorMiddleName As String = ""
	''' <summary>
	''' Отчество руководителя
	''' </summary>
	Public Property DirectorMiddleName() As String
		Get
			Return m_DirectorMiddleName
		End Get
		Set
			m_DirectorMiddleName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value.ToLower)
		End Set
	End Property
	Private m_DirectorLastName As String = ""
	''' <summary>
	''' Фамилия руководителя
	''' </summary>
	Public Property DirectorLastName() As String
		Get
			Return m_DirectorLastName
		End Get
		Set
			m_DirectorLastName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(value.ToLower)
		End Set
	End Property
	Private m_DirectorINN As String = ""
	''' <summary>
	''' ИНН Руководителя
	''' </summary>
	Public Property DirectorINN() As String
		Get
			Return m_DirectorINN
		End Get
		Set
			m_DirectorINN = value
		End Set
	End Property
	
	Private m_PrimaryOKVEDS As New InteropArrayList
	''' <summary>
	''' Коды ОКВЭД (помеченные осн. деятельностью)
	''' </summary>
	''' <value>Массив объектов</value>
	Public Property PrimaryOKVEDS() As InteropArrayList
		Get
			Return m_PrimaryOKVEDS
		End Get
		Set
			m_PrimaryOKVEDS = value
		End Set
	End Property

	Private m_SecondaryOKVEDS As New InteropArrayList
	''' <summary>
	''' Коды ОКВЭД (помеченные доп. деятельностью) 
	''' </summary>
	''' <value>Массив объектов</value>
	Public Property SecondaryOKVEDS() As InteropArrayList
		Get
			Return m_SecondaryOKVEDS
		End Get
		Set
			m_SecondaryOKVEDS = value
		End Set
	End Property
	
	Private m_OPF As String = ""
	''' <summary>
	''' Организационно-правовая форма. Полное наименование
	''' </summary>
	Public Property OPF() As String
		Get
			Return m_OPF
		End Get
		Set
			m_OPF = value
		End Set
	End Property
	
	Private m_ShortOPF As String = ""
	''' <summary>
	''' Краткое наименование организационно-правовой формы
	''' </summary>
	Public Property ShortOPF() As String
		Get
			Return m_ShortOPF
		End Get
		Set
			m_ShortOPF = value
		End Set
	End Property
	
	Private m_Address As AddressData
	''' <summary>
	''' Данные Адреса
	''' </summary>
	Public Property Address() As AddressData
		Get
			Return m_Address
		End Get
		Set
			m_Address = value
		End Set
	End Property
	
End Class


<Guid("a591a38a-93a3-45cc-8002-b92803d3f5bb")> _
	<ComVisible(True)> _
Public Interface IMyCollectionInterface
	
	Function Add(value As Object) As Integer
	Sub Clear()
	Function Contains(value As Object) As Boolean
	Function IndexOf(value As Object) As Integer
	Sub Insert(index As Integer, value As Object)
	Sub Remove(value As Object)
	Sub RemoveAt(index As Integer)
	ReadOnly Property Count As Integer
	
	<DispId(-4)> _
	Function GetEnumerator() As System.Collections.IEnumerator
	
	<DispId(0)> _
		<System.Runtime.CompilerServices.IndexerName("_Default")> _
	Default ReadOnly Property Item(index As Integer) As Object
	
End Interface

<ComVisible(True)> _
	<ClassInterface(ClassInterfaceType.None)> _
	<ComDefaultInterface(GetType(IMyCollectionInterface))> _
Public Class InteropArrayList
	Inherits System.Collections.ArrayList
	Implements IMyCollectionInterface
	#Region "IMyCollectionInterface Members"
	' COM friendly strong typed GetEnumerator
	<DispId(-4)> _
	Public Overloads Function GetEnumerator() As System.Collections.IEnumerator Implements IMyCollectionInterface.GetEnumerator
		Return MyBase.GetEnumerator()
	End Function
	#End Region
	
	Public Overloads Function Add(value As Object) As Integer Implements IMyCollectionInterface.Add
		Return MyBase.Add(value)
	End Function
	
	Public Overloads Sub RemoveAt(index As Integer) Implements IMyCollectionInterface.RemoveAt
		MyBase.RemoveAt(index)
	End Sub
	
	Public Overloads Sub Remove(value As Object) Implements IMyCollectionInterface.Remove
		MyBase.Remove(value)
	End Sub
	
	Public Overloads Sub Insert(index As Integer, value As Object) Implements IMyCollectionInterface.Insert
		MyBase.Insert(index,value)
	End Sub
	
	Public Overloads Sub Clear() Implements IMyCollectionInterface.Clear
		MyBase.Clear
	End Sub
	
	Public Overloads Function IndexOf(value As Object) As Integer Implements IMyCollectionInterface.IndexOf
		Return MyBase.IndexOf(value)
	End Function
	
	Public Overloads Function Contains(value As Object) As Boolean Implements IMyCollectionInterface.Contains
		Return MyBase.Contains(value)
	End Function
	
	Public ReadOnly Default Overloads Property Item(index As Integer) As Object Implements IMyCollectionInterface.Item
		Get
			Return MyBase.Item(index)	
		End Get
	End Property
	
	Public ReadOnly Overloads Property Count As Integer Implements IMyCollectionInterface.Count
		Get
			Return MyBase.Count
		End Get
	End Property
	
End Class


''' <summary>
''' Объект, представляющий запись данных по ОКВЭД
''' </summary>
<ComClass(),ComVisible(True)> _
Public Class OKVED
	
	Private m_BeginDate As Date
	''' <summary>
	''' Дата начала действия
	''' </summary>
	Public Property BeginDate() As DateTime
		Get
			Return m_BeginDate
		End Get
		Set
			m_BeginDate = value
		End Set
	End Property
	
	Private m_Code As String
	''' <summary>
	''' Код вида деятельности
	''' </summary>
	Public Property Code() As String
		Get
			Return m_Code
		End Get
		Set
			m_Code = value
		End Set
	End Property	
	
	Private m_Primary As Boolean
	''' <summary>
	''' Признак основной/дополнительный
	''' </summary>
	Public Property Primary() As Boolean
		Get
			Return m_Primary
		End Get
		Set
			m_Primary = value
		End Set
	End Property
	
End Class
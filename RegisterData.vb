Option Strict On
' Сделано в SharpDevelop.
' Пользователь: Админ
' Дата: 24.05.2017
' Время: 14:53
' 
' Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
'
#Region "Imports directives"

Imports System.Runtime.InteropServices
Imports System.Globalization.CultureInfo

#End Region

''' <summary>
''' Сведения об адресе организации
''' </summary>
<ComClass(AddressData.ClassId, AddressData.InterfaceId, _
          AddressData.EventsId), ComVisible(True)> _
Public Class AddressData
#Region "COM Registration"
Public Const ClassId As String _
    = "c2f1a215-e510-4742-9639-1b6bcdd077f0"
Public Const InterfaceId As String _
    = "5dd3e362-4985-47c3-bab0-aadc4a7291c6"
Public Const EventsId As String _
    = "af10f5b7-3b2e-4d26-bbfa-69eaa1e9436d"
#End Region

#Region "Properties"

Private m_Index As String = ""
''' <summary>
''' Индекс
''' </summary>
Public Property Index() As String
	Get
		Return m_Index
	End Get
	Set
		m_Index = value
	End Set
End Property


Private m_AdministrativeArea As String = ""
''' <summary>
''' Субъект (Область) 
''' </summary>
Public Property AdministrativeArea() As String
	Get
		Return m_AdministrativeArea
	End Get
	Set
		m_AdministrativeArea = value
	End Set
End Property


Private m_City As String = ""
''' <summary>
''' Город
''' </summary>
Public Property City() As String
	Get
		Return m_City
	End Get
	Set
		m_City = value
	End Set
End Property

Private m_Locality As String = ""
''' <summary>
''' Адрес (прим. ул. им Митрофана Седина)
''' </summary>
Public Property Locality() As String
	Get
		Return m_Locality
	End Get
	Set
		m_Locality = value
	End Set
End Property


Private Дом As String = ""
Private Владение As String = ""
Private Домовладение As String = ""
Private Корпус As String = ""
Private Строение As String = ""
Private Литера As String = ""
Private Сооружение As String = ""
Private Участок As String = ""
Private Квартира As String = ""
Private Офис As String = ""
Private Бокс As String = ""
Private Помещение As String = ""
Private Комната As String = ""
Private Этаж As String = ""

Private m_LocalityName As String = ""
''' <summary>
''' Дом/Владение 
''' </summary>
Public Property LocalityName() As String
	Get
		'Return m_LocalityName
		Dim st As String = ""
		If Not String.IsNullOrEmpty(Me.Дом) Then
			st += If(String.IsNullOrEmpty(st),""," ,")+"дом №"+Me.Дом
		End If
		If Not String.IsNullOrEmpty(Me.Владение) Then
			st += If(String.IsNullOrEmpty(st),""," ,")+"влад. "+Me.Владение
		End If
		If Not String.IsNullOrEmpty(Me.Домовладение) Then
			st += If(String.IsNullOrEmpty(st),""," ,")+"влад. "+Me.Домовладение
		End If
		If Not String.IsNullOrEmpty(Me.Корпус) Then
			st += If(String.IsNullOrEmpty(st),""," ,")+"корп. "+Me.Корпус
		End If
		If Not String.IsNullOrEmpty(Me.Строение) Then
			st += If(String.IsNullOrEmpty(st),""," ,")+"стр. "+Me.Строение
		End If
		If Not String.IsNullOrEmpty(Me.Литера) Then
			st += If(String.IsNullOrEmpty(st),""," ,")+"лит. "+Me.Литера
		End If
		If Not String.IsNullOrEmpty(Me.Сооружение) Then
			st += If(String.IsNullOrEmpty(st),""," ,")+"соор. "+Me.Сооружение
		End If
		If Not String.IsNullOrEmpty(Me.Участок) Then
			st += If(String.IsNullOrEmpty(st),""," ,")+"уч. "+Me.Участок
		End If
		If Not String.IsNullOrEmpty(Me.Офис) Then
			st +=If(String.IsNullOrEmpty(st),""," ,")+"оф. "+Me.Офис
		End If
		Return st
	End Get
	Set
		m_LocalityName = value
	End Set
End Property


#End Region

#Region "Methods"
''' <summary>
''' Конструктор объекта типа Адрес на основании переданной структуры данных
''' </summary>
''' <param name="address">Адресный объект в формате сервиса</param>
Public Sub New(address As АдресРФ)
	
	If Not String.IsNullOrEmpty(address.СубъектРФ) Then
		Me.AdministrativeArea = CurrentCulture.TextInfo.ToTitleCase(address.СубъектРФ.ToLower)
	End If
	
	If String.IsNullOrEmpty(address.Город) And Not String.IsNullOrEmpty(address.НаселПункт) Then
		Me.City = CurrentCulture.TextInfo.ToTitleCase(address.НаселПункт.ToLower)
	ElseIf Not String.IsNullOrEmpty(address.Город) Then	
		Me.City = CurrentCulture.TextInfo.ToTitleCase(address.Город.ToLower)
	End If

	If Not String.IsNullOrEmpty(address.Улица) Then 
		Me.Locality = CurrentCulture.TextInfo.ToTitleCase(address.Улица.ToLower)
	End If	
	
	Dim Abbr As Dictionary(Of String,String) = GetAbbreviation()
	For Each item As KeyValuePair(Of String, String) In Abbr
		Me.AdministrativeArea = Me.AdministrativeArea.Replace(item.Key,item.Value)
		'Me.City = Me.City.Replace(item.Key,item.Value)
		Me.City = ReplaceFullWords(Me.City,item.Key,item.Value)
		If Not Me.Locality.StartsWith(item.Key) Then
			'Me.Locality = Me.Locality.Replace(item.Key,item.Value)
			Me.Locality = ReplaceFullWords(Me.Locality,item.Key,item.Value)
		End If
	Next
	
	If address.ДопАдрЭл.Length > 0 Then
		For Each addrElem As АдресРФДопАдрЭл In address.ДопАдрЭл
			' Код индекса
			If addrElem.ТипАдрЭл = "10100000" Then
				Me.Index = addrElem.Значение
			End If
			If addrElem.Номер IsNot Nothing Then
				' номера записаны как "ДОМ 167", уберем ДОМ
				Dim Start As Integer = InStr(addrElem.Номер.Значение,".")
				If Start = 0 Then
					Start = InStr(addrElem.Номер.Значение," ")
				End If
				If Start>0 Then
					Dim FirstWord As String = Strings.Left(addrElem.Номер.Значение,Start)
					If addrElem.Номер.Значение.StartsWith(FirstWord) Then
						addrElem.Номер.Значение = addrElem.Номер.Значение.TrimStart(FirstWord.ToCharArray)
					End If
				End If
				Select Case addrElem.Номер.Тип
					Case "1010" '"Дом"
						Me.Дом = addrElem.Номер.Значение 
					Case "1020" '"Владение"
						Me.Владение = addrElem.Номер.Значение
					Case "1030" '"Домовладение"
						Me.Домовладение = addrElem.Номер.Значение
					Case "1050" '"Корпус"
						Me.Корпус = addrElem.Номер.Значение
					Case "1060" '"Строение"
						Me.Строение = addrElem.Номер.Значение
					Case "1080" '"Литера"
						Me.Литера = addrElem.Номер.Значение
					Case "1070" '"Сооружение"
						Me.Сооружение = addrElem.Номер.Значение
					Case "1040" '"Участок"
						Me.Участок = addrElem.Номер.Значение
					Case "2010" '"Квартира"
						Me.Квартира = addrElem.Номер.Значение
					Case "2030" '"Офис"
						Me.Офис = addrElem.Номер.Значение
					Case "2040" '"Бокс"
						Me.Бокс = addrElem.Номер.Значение
					Case "2020" '"Помещение"
						Me.Помещение = addrElem.Номер.Значение
					Case "2050" '"Комната"
						Me.Комната = addrElem.Номер.Значение
					Case "2060" '"Этаж"
						Me.Этаж = addrElem.Номер.Значение
					Case Else
						'do nothing
				End Select
			End If
		Next
	End If
	
End Sub

''' <summary>
''' Перегрузка конструктора, возвращающая пустой адрес в случае ИП
''' </summary>
Public Sub New()
	
End Sub

''' <summary>
''' Возвращает словарь сокращений, используемых в адресах
''' </summary>
''' <returns></returns>
Private Function GetAbbreviation() As Dictionary(Of String, String)
	
	Dim abbreviation As New Dictionary(Of String, String)
	abbreviation.Add("Абонентский ящик", "а/я")
	abbreviation.Add("Аал", "аал")
	abbreviation.Add("Автодорога", "автодорога")
	abbreviation.Add("Аллея", "аллея")
	abbreviation.Add("Автономный округ", "АО")
	abbreviation.Add("Автономная область", "Аобл")
	abbreviation.Add("Арбан", "арбан")
	abbreviation.Add("Аул", "аул")
	abbreviation.Add("Бульвар", "б-р")
	abbreviation.Add("Балка", "балка")
	abbreviation.Add("Берег", "берег")
	abbreviation.Add("Бугор", "бугор")
	abbreviation.Add("Бухта", "бухта")
	abbreviation.Add("Вал", "вал")
	abbreviation.Add("Волость", "волость")
	abbreviation.Add("Въезд", "въезд")
	abbreviation.Add("Выселки(ок)", "высел")
	abbreviation.Add("Город", "г")
	abbreviation.Add("Горка", "горка")
	abbreviation.Add("Городок", "городок")
	abbreviation.Add("городской поселок", "гп")
	abbreviation.Add("Гаражно-строит-ный кооператив", "гск")
	abbreviation.Add("Гаражно-строительный кооператив", "гск")
	abbreviation.Add("Деревня", "д")
	abbreviation.Add("Дачное нек", "днп")
	abbreviation.Add("Дачное некоммерческое партнерство", "днп")
	abbreviation.Add("Дом", "ДОМ")
	abbreviation.Add("Дорога", "дор")
	abbreviation.Add("Дачный поселок", "дп")
	abbreviation.Add("Железнодорожная будка", "ж/д_будка")
	abbreviation.Add("Железнодорожная казарма", "ж/д_казарм")
	abbreviation.Add("ж/д останов. (обгонный) пункт", "ж/д_оп")
	abbreviation.Add("Железнодорожная платформа", "ж/д_платф")
	abbreviation.Add("Железнодорожный пост", "ж/д_пост")
	abbreviation.Add("Железнодорожный разъезд", "ж/д_рзд")
	abbreviation.Add("Железнодорожная станция", "ж/д_ст")
	abbreviation.Add("Жилая зона", "жилзона")
	abbreviation.Add("Жилой район", "жилрайон")
	abbreviation.Add("Животноводческая точка", "жт")
	abbreviation.Add("Заезд", "заезд")
	abbreviation.Add("Заимка", "заимка")
	abbreviation.Add("Зона", "зона")
	abbreviation.Add("Казарма", "казарма")
	abbreviation.Add("Канал", "канал")
	abbreviation.Add("Квартал", "кв-л")
	abbreviation.Add("Километр", "км")
	abbreviation.Add("Кольцо", "кольцо")
	abbreviation.Add("Кордон", "кордон")
	abbreviation.Add("Коса", "коса")
	abbreviation.Add("Курортный поселок", "кп")
	abbreviation.Add("Край", "край")
	abbreviation.Add("Линия", "линия")
	abbreviation.Add("Леспромхоз", "лпх")
	abbreviation.Add("Местечко", "м")
	abbreviation.Add("Массив", "массив")
	abbreviation.Add("Маяк", "маяк")
	abbreviation.Add("Местность", "местность")
	abbreviation.Add("Микрорайон", "мкр")
	abbreviation.Add("Мост", "мост")
	abbreviation.Add("Мыс", "мыс")
	abbreviation.Add("Некоммерче", "н/п")
	abbreviation.Add("Некоммерческое партнерство", "н/п")
	abbreviation.Add("Набережная", "наб")
	abbreviation.Add("Населенный пункт", "нп")
	abbreviation.Add("Область", "обл")
	abbreviation.Add("Округ", "округ")
	abbreviation.Add("Остров", "остров")
	abbreviation.Add("Поселение", "п")
	abbreviation.Add("Поселок", "п")
	abbreviation.Add("Почтовое отделение", "п/о")
	abbreviation.Add("Планировочный район", "п/р")
	abbreviation.Add("Поселок и(при) станция(и)", "п/ст")
	abbreviation.Add("Парк", "парк")
	abbreviation.Add("Поселок городского типа", "пгт")
	abbreviation.Add("Переулок", "пер")
	abbreviation.Add("Переезд", "переезд")
	abbreviation.Add("Площадь", "пл")
	abbreviation.Add("Площадка", "пл-ка")
	abbreviation.Add("Платформа", "платф")
	abbreviation.Add("Погост", "погост")
	abbreviation.Add("Полустанок", "полустанок")
	abbreviation.Add("Починок", "починок")
	abbreviation.Add("Проспект", "пр-кт")
	abbreviation.Add("Причал", "причал")
	abbreviation.Add("Проезд", "проезд")
	abbreviation.Add("Промышленная зона", "промзона")
	abbreviation.Add("Просек", "просек")
	abbreviation.Add("Просека", "просека")
	abbreviation.Add("Проселок", "проселок")
	abbreviation.Add("Проток", "проток")
	abbreviation.Add("Протока", "протока")
	abbreviation.Add("Проулок", "проулок")
	abbreviation.Add("Район", "р-н")
	abbreviation.Add("Республика", "Респ")
	abbreviation.Add("Разъезд", "рзд")
	abbreviation.Add("Рабочий поселок", "рп")
	abbreviation.Add("Ряды", "ряды")
	abbreviation.Add("Село", "с")
	abbreviation.Add("Сельская администрация", "с/а")
	abbreviation.Add("Сельское муницип.образование", "с/мо")
	abbreviation.Add("Сельский округ", "с/о")
	abbreviation.Add("Сельское поселение", "с/п")
	abbreviation.Add("Сельсовет", "с/с")
	abbreviation.Add("Сад", "сад")
	abbreviation.Add("Сквер", "сквер")
	abbreviation.Add("Слобода", "сл")
	abbreviation.Add("Садовое неком-е товарищество", "снт")
	abbreviation.Add("Садовое товарищество", "снт")
	abbreviation.Add("Спуск", "спуск")
	abbreviation.Add("Станция", "ст")
	abbreviation.Add("Станица", "ст-ца")
	abbreviation.Add("Строение", "стр")
	abbreviation.Add("Территория", "тер")
	abbreviation.Add("тоннель", "тоннель")
	abbreviation.Add("Тракт", "тракт")
	abbreviation.Add("Тупик", "туп")
	abbreviation.Add("Улус", "у")
	abbreviation.Add("Улица", "ул")
	abbreviation.Add("Участок", "уч-к")
	abbreviation.Add("Фермерское", "ф/х")
	abbreviation.Add("Фермерское хозяйство", "ф/х")
	abbreviation.Add("Ферма", "ферма")
	abbreviation.Add("Хутор", "х")
	abbreviation.Add("Чувашия", "Чувашия")
	abbreviation.Add("Шоссе", "ш")
	abbreviation.Add("эстакада", "эстакада")	
	Return abbreviation

End Function

''' <summary>
''' Функция выполняет замену слов по вхождению только при совпадении целого слова.
''' Используется, чтобы не зависеть от регулярных выражений
''' </summary>
''' <param name="s">Входная строка</param>
''' <param name="oldWord">Искомое слово</param>
''' <param name="newWord">Слово для замены</param>
''' <returns>Обработанная строка</returns>
Private Function ReplaceFullWords(s As String, oldWord As String, newWord As String) As String
	If s Is Nothing Then
		Return Nothing
	End If
	Dim startIndex As Integer = 0
	' Where we start to search in s.
	Dim copyPos As Integer = 0
	' Where we start to copy from s to sb.
	Dim sb = New System.Text.StringBuilder()
	While True
		Dim position As Integer = s.IndexOf(oldWord, startIndex)
		If position = -1 Then
			If copyPos = 0 Then
				Return s
			End If
			If s.Length > copyPos Then
				' Copy last chunk.
				sb.Append(s.Substring(copyPos, s.Length - copyPos))
			End If
			Return sb.ToString()
		End If
		Dim indexAfter As Integer = position + oldWord.Length
		If (position = 0 OrElse Not Char.IsLetterOrDigit(s(position - 1))) AndAlso (indexAfter = s.Length OrElse Not Char.IsLetterOrDigit(s(indexAfter))) Then
			sb.Append(s.Substring(copyPos, position - copyPos)).Append(newWord)
			copyPos = position + oldWord.Length
		End If
		startIndex = position + oldWord.Length
	End While
	Return s
End Function
#End Region


End Class


Public Class Okopf
	
	Public Structure OPFData
		Dim Name As String
		Dim ShortName As String
	End Structure
	
	Private DataDict As Dictionary(Of String, OPFData)
	
	Public Sub New()
		dataDict = New Dictionary(Of String, OPFData)
		dataDict.Add("12300", New OPFData With {.Name = "Общество с ограниченной ответственностью", .ShortName = "ООО"})
		dataDict.Add("12247", New OPFData With {.Name = "Публичное акционерное общество", .ShortName = "ПАО"})
		dataDict.Add("12267", New OPFData With {.Name = "Непубличное акционерное общество", .ShortName = "ЗАО"})
		'Далее частью из ОКОПФ, частью эмпирическим путем по ответам сервиса
		dataDict.Add("42", New OPFData With {.Name = "Государственное предприятие", .ShortName = "ФГУП"})
		dataDict.Add("75203", New OPFData With {.Name = "Государственное бюджетное учреждение", .ShortName = "ГБУ"})
	End Sub
	
	Public Function GetData(Code As String) As OPFData
		Dim a As OPFData = New OPFData
		If dataDict.TryGetValue(Code, a) Then
			Return a        
		Else
            Return a'заглушка на возможный апгрейд
        End If
	End Function
	
End Class



''' <summary>
''' Информация о частях адреса согласно приказу ФНС ММВ-7-1/525 от 31.08.2011.
''' В настоящий момент не используется.
''' </summary>
Public Class TypeOfAddressObject
	
	Private Dim dataDict As Dictionary(Of String, String)
	
	Public Sub New()
		dataDict = New Dictionary(Of String, String)
		
		dataDict.Add("1010", "Дом")
		dataDict.Add("1020", "Владение")
		dataDict.Add("1030", "Домовладение")
		
		dataDict.Add("1050", "Корпус")
		dataDict.Add("1060", "Строение")
		dataDict.Add("1080", "Литера")
		dataDict.Add("1070", "Сооружение")
		dataDict.Add("1040", "Участок")
		
		dataDict.Add("2010", "Квартира")
		dataDict.Add("2030", "Офис")
		dataDict.Add("2040", "Бокс")
		dataDict.Add("2020", "Помещение")
		dataDict.Add("2050", "Комната")
		dataDict.Add("2060", "Этаж")
		'  Наши сокращения для поддержки обратной совместимости при парсинге.
		dataDict.Add("2010", "кв.")
		dataDict.Add("2030", "оф.")
		' Уточняющие объекты
		dataDict.Add("10100000", "Почтовый индекс")
		dataDict.Add("10200000", "Адресная точка")
		dataDict.Add("10300000", "Садовое товарищество")
		dataDict.Add("10400000", "Элемент улично-дорожной сети, планировочной структуры дополнительного адресного элемента")
		dataDict.Add("10500000", "Промышленная зона")
		dataDict.Add("10600000", "Гаражно-строительный кооператив")
		dataDict.Add("10700000", "Территория")
	End Sub
	
	Public Function GetNameOfObject(somekey As String) As String
		Dim name As String = ""
		If dataDict.TryGetValue(somekey, name) Then
            Return name
        Else
            Return ""
        End If
	End Function
	
End Class
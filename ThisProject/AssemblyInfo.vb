﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Requisites by INN")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Galeon")> 
<Assembly: AssemblyProduct("Requisites by INN")> 
<Assembly: AssemblyCopyright("Galeon © Microsoft 2017")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: ComVisible(True)> 

    
'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("f01ee301-4d3b-4759-98af-f64029734ab8")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 

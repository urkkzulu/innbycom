Option Strict On
' Сделано в SharpDevelop.
' Пользователь: Админ
' Дата: 23.05.2017
' Время: 17:30
' 
' Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
'
#Region "Imports directives"
Imports System.Runtime.InteropServices
#End Region

<ComClass(Worker.ClassId, Worker.InterfaceId, _
	Worker.EventsId), ComVisible(True)> _
Public Class Worker
	Implements IDisposable
	' To detect redundant calls
	Private disposed As Boolean = False
	#Region "COM Registration"
	Public Const ClassId As String _
		= "55341629-76af-499b-ae93-252c893a23bb"
	Public Const InterfaceId As String _
		= "1903d999-7ca5-4f47-a4fd-21316688d9be"
	Public Const EventsId As String _
		= "24a88e4d-2bc7-4ba1-8663-7da098f302e2"
	#End Region
	
	#Region "Properties"
	
	#End Region
	
	#Region "Methods"
	' IDisposable
	Protected Overridable Sub Dispose( _
		ByVal disposing As Boolean)
		If Not Me.disposed Then
			If disposing Then
				
			End If
			' Free your own state (unmanaged objects).
			' Set large fields to null.
		End If
		Me.disposed = True
	End Sub
	
	''' <summary>
	''' A public method that returns a string "HelloWorld".
	''' </summary>
	''' <returns></returns>
	''' <remarks></remarks>
	Public Function HelloWorld() As String
		Return "HelloWorld"
	End Function
	
	
	''' <summary>
	''' Функция возвращает объект, имеющий поля INN, KPP, etc.
	''' </summary>
	''' <param name="Login">Логин пользователя сервиса</param>
	''' <param name="Password">Пароль пользователя сервиса</param>
	''' <param name="INN">ИНН</param>
	''' <returns>Объект типа QueryResult</returns>
	Public Function GetRequisitesByINN(Login As String, Password As String, INN As String) As QueryResult
		'Сервис принимает запросы только с Basic Authorization, иначе возвращает 401
		
		'Dim NetService As New RequisitesWebServiceEndpointImpl7Service With {
		'    .Credentials = New Net.NetworkCredential With {.UserName = Login, .Password = Password}
		'}''' Работает Только в VS
		
		Dim Credentials = New Net.NetworkCredential With {.UserName = Login, .Password = Password}
		Dim NetService = New RequisitesWebServiceEndpointImpl7Service With { .Credentials = Credentials}
		
		Dim Result As New QueryResult
		Dim Response As New Object 'это или РеквизитыЮрЛица или РеквизитыИП 
		Try
			If Len(INN)=10 Then
				Response = NetService.getCorporationRequisitesByINN(INN, "БухгалтерияПредприятия")
			ElseIf Len(INN)=12 Then
				Response = NetService.getEntrepreneurRequisitesByINN(INN,"БухгалтерияПредприятия")
			Else
				Result.ErrorHasOccurred = True
				Result.ErrorMessage = "Недопустимая длина ИНН " & INN
			End If	
		Catch ex As Exception' Неверные рег.данные или ошибка сети.
			Result.ErrorHasOccurred = True
			Result.ErrorMessage = ex.Message
		End Try
		
		If Response IsNot Nothing Then
			Try
				ParseResult(Result,Response)
			Catch ex As Exception' Неверные рег.данные или ошибка сети.
				Result.ErrorHasOccurred = True
				Result.ErrorMessage = ex.Message
			End Try	
		ElseIf Not Result.ErrorHasOccurred Then
			Result.ErrorHasOccurred = True
			Result.ErrorMessage = "Не удалось найти данные для заполнения реквизитов по ИНН " & INN
		End If
		
		Return Result
		
	End Function
	
	''' <summary>
	''' Функция выполняет разбор ответа сервиса  
	''' </summary>
	''' <param name="Result">Результат разбора</param>
	''' <param name="SrvResponse">Ответ сервиса. Имеет тип РеквизитыЮрЛица или РеквизитыИП</param>
	Private Sub ParseResult(Result As QueryResult, SrvResponse As Object)
		If TypeOf SrvResponse Is РеквизитыЮрЛица Then
			ParseUL(Result,CType(SrvResponse,РеквизитыЮрЛица))
		ElseIf TypeOf SrvResponse Is РеквизитыИП Then
			ParseFL(Result,CType(SrvResponse,РеквизитыИП))
		End If
	End Sub
	
	''' <summary>
	''' Функция выполняет разбор ответа сервиса, в случае реквизитов юрлица
	''' </summary>
	''' <param name="Qr">Результат разбора</param>
	''' <param name="Resp">Ответ сервиса</param>
	Private Sub ParseUL(Qr As QueryResult, Resp As РеквизитыЮрЛица)
		Qr.IsCompany = True
		Qr.INN = Resp.ИНН
		Qr.KPP = Resp.КПП
		Qr.OGRN = Resp.ОГРН
		Qr.Title = Resp.СвНаимЮЛ.НаимЮЛСокр
		Qr.FullTitle = Resp.СвНаимЮЛ.НаимЮЛПолн
    	If Not IsNothing(Resp.СвУправлДеят.СведДолжнФЛ) AndAlso Resp.СвУправлДеят.СведДолжнФЛ.Length>=1 Then
    		Dim d As СведДолжнФЛ = Resp.СвУправлДеят.СведДолжнФЛ(0)
    		If Not IsNothing(d.ФИО) Then
    			If Not String.IsNullOrEmpty(d.ФИО.Имя) Then
    				Qr.DirectorFirstName = d.ФИО.Имя
    			End If
    			If Not String.IsNullOrEmpty(d.ФИО.Отчество) Then
    				Qr.DirectorMiddleName = d.ФИО.Отчество
    			End If
    			If Not String.IsNullOrEmpty(d.ФИО.Фамилия) Then
    				Qr.DirectorLastName = d.ФИО.Фамилия
    			End If
    		End If
    		If Not String.IsNullOrEmpty(d.НаимДолжн) Then
    			Qr.DirectorPosition = d.НаимДолжн
    		End If
	   		If Not String.IsNullOrEmpty(d.ИННФЛ) Then
    			Qr.DirectorINN = d.ИННФЛ	
    		End If
    	End If
		For Each okv As СвОКВЭД In Resp.СвОКВЭД
			If okv.ПрОснДоп = "1" Then
				Qr.PrimaryOKVEDS.Add(New OKVED With {.BeginDate = okv.ДатаНачДейств,.Code = okv.КодОКВЭД,.Primary=True})
			Else
				Qr.SecondaryOKVEDS.Add(New OKVED With {.BeginDate = okv.ДатаНачДейств,.Code = okv.КодОКВЭД,.Primary=False})
			End If
		Next
		If Not String.IsNullOrEmpty(Resp.СвНаимЮЛ.ОПФ.КодОПФ) Then
			Dim opf As New Okopf
			Dim d As Okopf.OPFData
			d = opf.GetData(Resp.СвНаимЮЛ.ОПФ.КодОПФ)
			If Not String.IsNullOrEmpty(d.Name) Then
				Qr.OPF = d.Name
				Qr.ShortOPF = d.ShortName
			Else
				Qr.ShortOPF = ""
				Qr.OPF = Resp.СвНаимЮЛ.ОПФ.ПолнНаимОПФ
			End If
		End If
		If Resp.СвАдрес.Адрес IsNot Nothing Then
			Qr.Address = New AddressData(CType(Resp.СвАдрес.Адрес.Состав,АдресРФ))
		End If
	End Sub
	
	''' <summary>
	''' Функция выполняет разбор ответа сервиса, в случае реквизитов ИП
	''' </summary>
	''' <param name="Qr">Результат разбора</param>
	''' <param name="Resp">Ответ сервиса</param>
    Private Sub ParseFL(Qr As QueryResult, Resp As РеквизитыИП)
    	Qr.IsEntrepreneur = True
    	Qr.INN = Resp.ИННФЛ
    	Qr.OGRN = Resp.ОГРН
    	If Not IsNothing(Resp.СвФЛ.ФИОРус) Then
    		If Not String.IsNullOrEmpty(Resp.СвФЛ.ФИОРус.Имя) Then
    			Qr.FirstName = Resp.СвФЛ.ФИОРус.Имя
    		End If
    		If Not String.IsNullOrEmpty(Resp.СвФЛ.ФИОРус.Отчество) Then
    			Qr.MiddleName = Resp.СвФЛ.ФИОРус.Отчество 
    		End If
    		If Not String.IsNullOrEmpty(Resp.СвФЛ.ФИОРус.Фамилия) Then
    			Qr.LastName = Resp.СвФЛ.ФИОРус.Фамилия
    		End If
    		Qr.Title = Qr.FirstName +" "+Qr.MiddleName+" "+Qr.LastName
    		Qr.FullTitle = "Индивидуальный предприниматель "+Qr.Title
    	End If
    	Qr.Sex = CInt(Resp.СвФЛ.Пол)
    	Qr.Address = New AddressData()
    End Sub
	
	Public Sub Quit()
		Me.Dispose()
	End Sub
	#End Region
	
	#Region "Events"
	
	#End Region
	
	#Region " IDisposable Support "
	' This code added by Visual Basic to 
	' correctly implement the disposable pattern.
	Public Sub Dispose() Implements IDisposable.Dispose
		' Do not change this code. 
		' Put cleanup code in
		' Dispose(ByVal disposing As Boolean) above.
		Dispose(True)
		GC.SuppressFinalize(Me)
	End Sub
	
	Protected Overrides Sub Finalize()
		' Do not change this code. 
		' Put cleanup code in
		' Dispose(ByVal disposing As Boolean) above.
		Dispose(False)
		MyBase.Finalize()
	End Sub
	#End Region
End Class
